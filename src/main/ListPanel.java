package main;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.sql.Date;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;

public class ListPanel extends JPanel {
	
	class CustomTableModel extends DefaultTableModel {
		public CustomTableModel(String[] str, int par) {
			super(str, par);
		}
		@Override
		public boolean isCellEditable(int x, int y) {
			if(y == 0 || y == 7) {
				return false;
			}else return true;
		}
		
		@Override
		public void setValueAt(Object value, int x, int y) {
			super.setValueAt(value, x, y);
			
			String stockCode = (String) this.getValueAt(x, 0);			
			String columnNameInDB = this.getColumnName(y);
			
			Database.executeUpdateStatement("update stok_bilgisi set "+columnNameInDB+"=\""+value+"\" where StokKodu=\""+stockCode+"\"");

		}
	}

	class CustomTable extends JTable {
		public CustomTable(DefaultTableModel tableModel) {
			super(tableModel);
		}

		@Override
		public void valueChanged(ListSelectionEvent e) {
			super.valueChanged(e);
			
		}
	}
	
	private JTextField searchField;
	private JButton searchButton;
	private JButton showAllButton;
	private JComboBox<String> searchColumnComboBox;
	
	private CustomTable table;
	private DefaultTableModel tableModel;
	
	public ListPanel(InputPanel inputPanel) {
		
		JPanel searchPanel = new JPanel();
		FlowLayout fl = new FlowLayout();
		searchPanel.setLayout(fl);
		
		searchField = new JTextField();
		searchField.setActionCommand("Search");
		searchField.setPreferredSize(new Dimension(200, searchField.getPreferredSize().height));
		searchPanel.add(searchField);
		
		searchColumnComboBox = new JComboBox<String>();
		searchColumnComboBox.addItem("StokKodu");
		searchColumnComboBox.addItem("StokAdi");
		searchColumnComboBox.addItem("StokTipi");
		searchColumnComboBox.addItem("StokBirimi");
		searchColumnComboBox.addItem("Barkod");
		searchColumnComboBox.addItem("KDV");
		searchColumnComboBox.addItem("Aciklama");
		searchColumnComboBox.addItem("OlusturmaTarihi");
		searchPanel.add(searchColumnComboBox);
		
		searchButton = new JButton("Ara");
		searchButton.setActionCommand("Search");
		searchPanel.add(searchButton);
		
		showAllButton = new JButton("Tum Satirlari Listele");
		showAllButton.setActionCommand("Show All");
		searchPanel.add(showAllButton);
		
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		this.add(searchPanel);
		
		
		tableModel = new CustomTableModel(new String[]{"StokKodu", "StokAdi", "StokTipi",
				"StokBirimi", "Barkod", "KDV", "Aciklama", "OlusturmaTarihi"}, 0);
		table = new CustomTable(tableModel);
		table.setRowHeight(20);
		table.getColumnModel().getColumn(6).setPreferredWidth(200);
		
		table.getColumnModel().getColumn(2).setCellEditor(new DefaultCellEditor(new JComboBox<Integer>(inputPanel.getStockTypeComboBox().getModel())));
		table.getColumnModel().getColumn(3).setCellEditor(new DefaultCellEditor(new JComboBox<String>(inputPanel.getUnitComboBox().getModel())));
		table.getColumnModel().getColumn(5).setCellEditor(new DefaultCellEditor(new JComboBox<Double>(inputPanel.getVatComboBox().getModel())));
		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setPreferredSize(new Dimension(1000, 500));
		this.add(scrollPane);
		table.setFillsViewportHeight(true);
		
		setVisible(true);
	}
	
	public void addRow(String code, String name, int type, String unit, String barcode, double vat, String desc, Date date) {
		tableModel.addRow(new Object[]{code, name, type, unit, barcode, vat, desc, date});
	}
	
	public void addRow(StockCardModel row) {
		tableModel.addRow(new Object[] {row.getStockCode(), row.getStockName(), row.getStockType(), row.getStockUnit(),
				row.getBarcode(), row.getVat(), row.getDescription(), row.getDate()});
	}

	public String getCurrentSelectionStockCode() {
		return (String) tableModel.getValueAt(table.getSelectedRow(), 0);
	}
	
	public void clear() {
		while(tableModel.getRowCount() > 0) {
			tableModel.removeRow(0);
		}
	}

	public String getSearchString() {
		return searchField.getText();
	}
	
	public String getSearchColumn() {
		return (String)searchColumnComboBox.getSelectedItem();
	}
	
	public void populateTable(ArrayList<StockCardModel> rows) {
		this.clear();
		for(StockCardModel row : rows) {
			this.addRow(row);
		}
	}

	public JButton getSearchButton() {
		return searchButton;
	}
	public JTextField getSearchField() {
		return searchField;
	}
	public JButton getShowAllButton() {
		return showAllButton;
	}

}
