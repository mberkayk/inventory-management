package main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;

public class FrameController implements ActionListener {

	private Database db;
	private InputPanel inputPanel;
	private ListPanel listPanel;
	private MainWindow mainWindow;
	
	public FrameController(MainWindow mw, InputPanel ip, ListPanel lp) {
		this.mainWindow = mw;
		this.inputPanel = ip;
		this.listPanel = lp;
		
		for(JButton button : inputPanel.getActionButtons()) {
			button.addActionListener(this);
		}
		
		listPanel.getShowAllButton().addActionListener(this);
		listPanel.getSearchButton().addActionListener(this);
		listPanel.getSearchField().addActionListener(this);
		
		db  = new Database();
	}
	
	public ArrayList<StockCardModel> getAllRows(){
		ResultSet rs = Database.executeQueryStatement("select * from stok_bilgisi");
		
		return getCardModelFromResultSet(rs);
	}
	
	public ArrayList<StockCardModel> getCardModelFromResultSet(ResultSet rs){
		ArrayList<StockCardModel> rows = new ArrayList<StockCardModel>();
		try {
			while(rs.next()) {
				rows.add(new StockCardModel(rs.getString("StokKodu"), rs.getString("StokAdi"), rs.getInt("StokTipi"),
						rs.getString("StokBirimi"), rs.getString("Barkod"), rs.getDouble("KDV"), rs.getString("Aciklama"), rs.getDate("OlusturmaTarihi")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rows;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()) {
		case "Add Row":
			new AddCommand(inputPanel.getRow(), listPanel).execute();
			break;
			
		case "Copy Row":
			String newStockCode = JOptionPane.showInputDialog(mainWindow,"Stok kodu girin:", null);
			try {
				if(Database.executeQueryStatement("select * from stok_bilgisi where StokKodu=\"" + newStockCode + "\"").next() == false) {
					Database.executeUpdateStatement("insert into stok_bilgisi (StokKodu, StokAdi, StokTipi, StokBirimi, Barkod, KDV, Aciklama, OlusturmaTarihi) "
						+ "select \""+ newStockCode +"\", StokAdi, StokTipi, StokBirimi, Barkod, KDV, Aciklama, OlusturmaTarihi from stok_bilgisi where StokKodu=\"" + listPanel.getCurrentSelectionStockCode() + "\"");
				}else {
					JOptionPane.showMessageDialog(mainWindow, "Girdiginiz stok kodu veritabaninda mevcut!");
					break;
				}
				listPanel.populateTable(getAllRows());
				break;
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			
		case "Delete Row":
			Database.executeUpdateStatement("delete from stok_bilgisi where StokKodu=\""+listPanel.getCurrentSelectionStockCode()+"\"");
			listPanel.populateTable(getAllRows());
			break;
			
		case "Search":
			ResultSet rs = Database.executeQueryStatement("select * from stok_bilgisi where " 
					+ listPanel.getSearchColumn() +"=\"" + listPanel.getSearchString() +"\"");	
			listPanel.populateTable(getCardModelFromResultSet(rs));
			break;
			
		case "Show All":
			listPanel.populateTable(this.getAllRows());
			break;
		}
	}

	public void closeConnection() {
		db.closeConnection();
	}
	
}
