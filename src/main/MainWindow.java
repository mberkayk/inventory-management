package main;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BoxLayout;
import javax.swing.JFrame;

public class MainWindow extends JFrame implements WindowListener {
	
	private FrameController frameController;

	private InputPanel inputPanel;
	private ListPanel listPanel;
	
	public MainWindow() {
		super("Inventory Management");
		super.setSize(600, 400);
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setLayout(new BoxLayout(super.getContentPane(), BoxLayout.LINE_AXIS));
		
		
		inputPanel = new InputPanel();
		this.add(inputPanel);

		listPanel = new ListPanel(inputPanel);
		this.add(listPanel);
		
		frameController = new FrameController(this, inputPanel, listPanel);

		listPanel.populateTable(frameController.getAllRows());
		
		super.pack();
		super.setVisible(true);
	}
	
	public static void main(String[] args) {
		new MainWindow();
	}

	@Override
	public void windowClosing(WindowEvent e) {
		frameController.closeConnection();
	}
	
	
	
	@Override
	public void windowActivated(WindowEvent e) {}
	@Override
	public void windowClosed(WindowEvent e) {}
	@Override
	public void windowDeactivated(WindowEvent e) {}
	@Override
	public void windowDeiconified(WindowEvent e) {}
	@Override
	public void windowIconified(WindowEvent e) {}
	@Override
	public void windowOpened(WindowEvent e) {}
	
	
}
