package main;

import java.sql.Date;

public class StockCardModel {


	private String stockCode;
	private String stockName;
	private int stockType;
	private String stockUnit;
	private String barcode;
	private double vat;
	private String description;
	private Date date;
	
	public StockCardModel(String code, String name, int type, String unit, String barcode, double vat, String desc, Date date) {
		this.stockCode = code;
		this.stockName = name;
		this.stockType = type;
		this.stockUnit = unit;
		this.barcode = barcode;
		this.vat = vat;
		this.description = desc;
		this.date = date;
	}
	
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public String getStockName() {
		return stockName;
	}
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	public int getStockType() {
		return stockType;
	}
	public void setStockType(int stockType) {
		this.stockType = stockType;
	}
	public String getStockUnit() {
		return stockUnit;
	}
	public void setStockUnit(String stockUnit) {
		this.stockUnit = stockUnit;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public double getVat() {
		return vat;
	}
	public void setVat(double vat) {
		this.vat = vat;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public String getValuesAsString() {		
		return "\""+ stockCode + "\", \"" + stockName +"\", \"" + stockType + "\", \"" + stockUnit + "\", \""
		+ barcode + "\", \"" + vat + "\", \"" + description + "\", \"" + date +"\"";
	}
	
	public void save() {
		Database.executeUpdateStatement("insert into stok_bilgisi values(" + this.getValuesAsString() + ")");
	}
	
}
