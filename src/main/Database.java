package main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;


public class Database {
	
	private static Connection conn;
	
	public Database() {
		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/inventory?autoReconnect=true&useSSL=false", "root", "pass123");
		}catch (SQLException e){
			e.printStackTrace();
		}
	}
	
	public void closeConnection() {
		try {
			conn.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static ResultSet executeQueryStatement(String str) {
		try {
			return conn.prepareStatement(str).executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static int executeUpdateStatement(String str) {
		try {
			return conn.prepareStatement(str).executeUpdate(str);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}
}
 