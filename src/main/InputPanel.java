package main;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class InputPanel extends JPanel {

	private JTextField stockCodeField;
	private JTextField stockNameField;
	private JComboBox<Integer> stockTypeComboBox;
	private JComboBox<String> unitComboBox;
	private JTextField barcodeField;
	private JComboBox<Double> vatComboBox;
	private JTextArea descriptionArea;
	private JFormattedTextField insertDateField;
	
	private JButton addButton;
	private JButton copyButton;
	private JButton deleteButton;
	
	public InputPanel() {
		super.setPreferredSize(new Dimension(300, 500));
		GridBagLayout layout = new GridBagLayout();
		this.setLayout(layout);
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridy = 0;
		gbc.insets = new Insets(2, 0, 2, 0);
		gbc.weightx = 1;
		gbc.weightx = 0;
		this.add(new JLabel("Stok Kodu:"), gbc);
		stockCodeField = new JTextField();
		gbc.gridy = 0;
		gbc.weightx = 1;
		this.add(stockCodeField, gbc);
		

		gbc.gridy = 1;
		gbc.weightx = 0;
		this.add(new JLabel("Stok Adi:"), gbc);
		stockNameField = new JTextField();
		gbc.weightx = 1;
		this.add(stockNameField, gbc);
		
		gbc.gridy = 2;
		gbc.weightx = 0;
		this.add(new JLabel("Stok Tipi:"), gbc);
		stockTypeComboBox = new JComboBox<Integer>();
		stockTypeComboBox.addItem(0);
		stockTypeComboBox.addItem(1);
		stockTypeComboBox.addItem(2);
		stockTypeComboBox.addItem(3);
		gbc.weightx = 1;
		this.add(stockTypeComboBox, gbc);
		
		gbc.gridy = 3;
		gbc.weightx = 0;
		this.add(new JLabel("Stok Birimi:"), gbc);
		unitComboBox = new JComboBox<String>();
		unitComboBox.addItem("Adet");
		unitComboBox.addItem("Metre");
		unitComboBox.addItem("Kilogram");
		gbc.weightx = 1;
		this.add(unitComboBox, gbc);
		
		gbc.gridy = 4;
		gbc.weightx = 0;
		this.add(new JLabel("Barkod:"), gbc);
		barcodeField = new JTextField();
		gbc.weightx = 1;
		this.add(barcodeField, gbc);
		
		gbc.gridy = 5;
		gbc.weightx = 0;
		this.add(new JLabel("KDV Tipi"), gbc);
		vatComboBox = new JComboBox<Double>();
		vatComboBox.addItem(0.00);
		vatComboBox.addItem(0.08);
		vatComboBox.addItem(0.19);
		gbc.weightx = 1;
		this.add(vatComboBox, gbc);
		
		gbc.gridy = 6;
		gbc.weighty = 1;
		gbc.weightx = 0;
		this.add(new JLabel("Aciklama:"), gbc);
		descriptionArea = new JTextArea();
		descriptionArea.setLineWrap(true);
		gbc.weightx = 0;
		this.add(descriptionArea, gbc);
		
		gbc.gridy = 7;
		gbc.weighty = 0;
		gbc.weightx = 0;
		this.add(new JLabel("Olusturma Tarihi:"), gbc);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    insertDateField = new JFormattedTextField(dateFormat);
		gbc.weightx = 1;
		this.add(insertDateField, gbc);
		
		addButton = new JButton("Satir Ekle");
		addButton.setActionCommand("Add Row");
		gbc.gridy = 9;
		gbc.gridx = 0;
		gbc.gridwidth = 2;
		this.add(addButton, gbc);
		
		copyButton = new JButton("Satir Kopyala");
		copyButton.setActionCommand("Copy Row");
		gbc.gridy = 10;
		gbc.gridx = 0;
		gbc.gridwidth = 2;
		this.add(copyButton, gbc);
		
		deleteButton = new JButton("Satiri Sil");
		deleteButton.setActionCommand("Delete Row");
		gbc.gridy = 11;
		gbc.gridx = 0;
		gbc.gridwidth = 2;
		this.add(deleteButton, gbc);
	}
	
	public StockCardModel getRow() {
		return new StockCardModel(stockCodeField.getText(), stockNameField.getText(), (int) stockTypeComboBox.getSelectedItem(), (String)unitComboBox.getSelectedItem(),
				(String)barcodeField.getText(), (double)vatComboBox.getSelectedItem(), (String)descriptionArea.getText(), Date.valueOf(insertDateField.getText()));
	}
	
	public String getValuesAsString() {		
		return "\""+stockCodeField.getText() + "\", \"" + stockNameField.getText()+"\", \""
		+ stockTypeComboBox.getSelectedItem() + "\", \"" + unitComboBox.getSelectedItem() + "\", \""
		+ barcodeField.getText() + "\", \"" + vatComboBox.getSelectedItem() + "\", \"" + descriptionArea.getText() + "\", \"" + insertDateField.getText()+"\"";
	}
	
	public JComboBox<Integer> getStockTypeComboBox() {return stockTypeComboBox;}
	public JComboBox<String> getUnitComboBox(){return unitComboBox;}
	public JComboBox<Double> getVatComboBox(){return vatComboBox;}
	
	public JButton[] getActionButtons() {
		JButton[] arr = {this.addButton, this.deleteButton, this.copyButton}; 
		return arr;
		
	}
}
