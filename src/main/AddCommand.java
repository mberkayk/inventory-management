package main;

public class AddCommand implements Command {

	private StockCardModel row;
	private ListPanel lp;
	
	public AddCommand(StockCardModel row, ListPanel lp) {
		this.row = row;
		this.lp = lp;
	}

	@Override
	public void execute() {

		row.save();
		lp.addRow(row);
		
		
	}
	
}
